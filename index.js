// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/


// [SECTION] Inserting Documents (CREATE)


/*

	Syntax:
		-db.collectionName.insertOne({object});

*/

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age:21,
	contact: {
		phone: "87654321",
		email: "janedoe@mail.com"
	}
	courses: ["CSS", "Javascript", "Python"];
	department: "none";
});



// Insert many
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87000",
			email: "stephenhawking@mail.com"
		},
		couses: ["Python", "React", "PHP"],
		department: "none"
	},

	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 76,
		contact: {
			phone: "8784561",
			email: "neilamstrong@gmail.com"
		},
		couses: ["React", "Laravel", "MongoDB"],
		department: "none"

	}
]);

// [SECTION] Finding Documents (read)

/*
	db.collectionName.find();
	db.collectionName.find({ field: value})
*/

db.users.find();
db.users.find({ firstName: "Stephen"});


// ------------------------

db.users.deleteOne({
	firstName: "Stephen",
})

